# What is it?

A very simple receiver/sender for 433 MHz RF signals which uses the
pigpio library for Python.

Its main selling point is that it will record and transmit the raw
data from the 433 MHz transceiver and does not need to understand the
RF protocol.


# How to use

Connect a 433 MHz receiver and transmitter to your Raspberry Pi.

I have used the following transmitter/receiver pair successfully on a
Raspberry Pi B: https://www.kjell.com/se/sortiment/el-verktyg/elektronik/fjarrstyrning/sandar-och-mottagarmodul-433-mhz-p88905

It is very likely that similar 433 MHz modules will work as well.

# Why

I bought a pair of remotely controlled electric sockets which operated
at 433 MHz but which had a protocol which wasn't recognized by for
example rpi_rf. After spending a few hours trying to figure out if it
was possible to get rpi_rf to recognize the protocol I figured it was
easier to simply record the waveform and send it unchanged. It is not
very elegant, but at least it works.

# Main drawback

It does not understand the protocol itself and is thus likely to send
a suboptimal RF signal that may be harder to decode correctly for the
receiver. This could for example be a problem at longer range.

## Example usage, how to record a signal from GPIO pin 4

python3 pi_rf_learn_pattern.py 4 button_a_off.rfraw

## Example usage, how to send this signal to GPIO pin 22

python3 pi_rf_send_pattern.py 4 button_a_off.rfraw

# Author

Andreas Ehliar

# License

MIT license (see the .py files)

