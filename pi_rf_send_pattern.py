#!/usr/bin/env python3

"""

A simple script to send an RF signal captured by rf_pi_learn_pattern.py.

Uses pigpio to send the waveform to ensure that the timing is
adequate. Tested on Raspberry Pi model B.

Copyright 2018 Andreas Ehliar <andreas@ehliar.se>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""

import atexit
import sys
import time

import pigpio

def process_pattern(gpiopin, filename):
    """ Read a file output from rf_pi_learn_pattern.py and convert it
        to a format that pigpio can understand. """
    sig = []
    with open(filename, 'r') as fid:
        sig.append(pigpio.pulse(1 << gpiopin, 0, 100000)) # 100 us idle pattern preamble
        prev_val = 1
        prevtime = None
        for line in fid.readlines():
            vals = line.split()
            timestamp = int(vals[1])
            if prevtime is None:
                prevtime = timestamp - 100000
            if prev_val == 0:
                sig.append(pigpio.pulse(0, 1 << gpiopin, timestamp - prevtime))
            else:
                sig.append(pigpio.pulse(1 << gpiopin, 0, timestamp - prevtime))
            prevtime = timestamp
            prev_val = int(vals[0])
        sig.append(pigpio.pulse(1 << gpiopin, 0, 100000)) # End signal with 100 us idle pattern
    return sig

def send_pattern(gpiopin, pattern):
    """ Send the specified pattern on the specified pin. """
    p = pigpio.pi()
    p.wave_clear()
    p.set_mode(gpiopin, pigpio.OUTPUT)

    def atexit_handler():
        """
        It is not very polite to leave the sender in a state where
        we send for example a continuous 0 on the output. Try to
        avoid this at all costs by tri-stating the output
        signal. (Tri-stating the signal should also reduce, but not
        eliminate problems caused by specifying the wrong GPIO pin
        in many situations.)
        """
        p.set_mode(gpiopin, pigpio.INPUT)
        p.stop()
        print("\nClosed GPIO pin\n")

    atexit.register(atexit_handler)

    p.wave_add_generic(pattern)
    wid = p.wave_create()
    print("Sending RF pattern")
    p.wave_send_once(wid)
    count = 0
    while p.wave_tx_busy():
        print(".", end="", flush=True)
        count += 1
        time.sleep(0.1)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("ERROR: Exactly two arguments must be given")
        print("\n  Usage: rf_pi_send_pattern.py pin_number pattern_name.rfraw")
        print("\n  Example: rf_pi_send_pattern.py 22 transmitter_button_a_on.rfraw\n")
        sys.exit(1)
    else:
        gpiopin = int(sys.argv[1])
        pattern = process_pattern(gpiopin, sys.argv[2])
        send_pattern(gpiopin, pattern)
