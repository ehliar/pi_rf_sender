#!/usr/bin/env python3

"""

A simple script to capture the RF signal from a 433 MHz receiver
connected to a GPIO pin. No attempt is made to try to decode the
protocol, instead the raw data is captured using pigpio which allows
for high speed sampling with decent precision. Tested on Raspberry
Pi model B.

Copyright 2018 Andreas Ehliar <andreas@ehliar.se>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""

import sys
import time

import pigpio

def learn_pattern(gpiopin, filename):
    """ Open the GPIO pin specified as an input and
        store all events in the specified file. """
    with open(filename, 'w') as fid:
        events = 0
        def handle_event(_, level, tick):
            """ Callback function for pigpio """
            fid.write('%d %d\n' % (level, tick))
            events += 1

        p = pigpio.pi()
        p.callback(gpiopin, pigpio.EITHER_EDGE, handle_event)
        print("Press the RF button you want to record")
        for i in range(1, 6):
            time.sleep(1)
            print("%d/5 seconds" % i)
        print("Finished (%d events recorded)!" % events)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("ERROR: Exactly two arguments must be given")
        print("\n  Usage: rf_pi_learn_pattern.py pin_number pattern_name.rfraw")
        print("\n  Example: rf_pi_learn_pattern.py 4 transmitter_button_a_on.rfraw\n")
        sys.exit(1)
    else:
        learn_pattern(int(sys.argv[1]), sys.argv[2])
